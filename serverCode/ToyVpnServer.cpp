/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#ifdef __linux__

// There are several ways to play with this program. Here we just give an
// example for the simplest scenario. Let us say that a Linux box has a
// public IPv4 address on eth0. Please try the following steps and adjust
// the parameters when necessary.
//
// # Enable IP forwarding
// echo 1 > /proc/sys/net/ipv4/ip_forward
//
// # Pick a range of private addresses and perform NAT over eth0.
// iptables -t nat -A POSTROUTING -s 10.0.0.0/8 -o eth0 -j MASQUERADE
//
// # Create a TUN interface.
// ip tuntap add dev tun0 mode tun
//
// # Set the addresses and bring up the interface.
// ifconfig tun0 10.0.0.1 dstaddr 10.0.0.2 up
//
// # Create a server on port 8000 with shared secret "test".
// ./ToyVpnServer tun0 8000 test -m 1400 -a 10.0.0.2 32 -d 8.8.8.8 -r 0.0.0.0 0
//
// This program only handles a session at a time. To allow multiple sessions,
// multiple servers can be created on the same port, but each of them requires
// its own TUN interface. A short shell script will be sufficient. Since this
// program is designed for demonstration purpose, it performs neither strong
// authentication nor encryption. DO NOT USE IT IN PRODUCTION!

#include <net/if.h>
#include <linux/if_tun.h>

static int get_interface(char *name)
{
    int interface = open("/dev/net/tun", O_RDWR );

    ifreq ifr;
    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
    strncpy(ifr.ifr_name, name, sizeof(ifr.ifr_name));

    if (ioctl(interface, TUNSETIFF, &ifr)) {
        perror("Cannot get TUN interface");
        exit(1);
    }

    return interface;
}

#else

#error Sorry, you have to implement this part by yourself.

#endif

//-----------------------------------------------------------------------------
void vpnprotocol (int sock,int inter)
{
	// Put the tunnel into non-blocking mode.
	//fcntl(sock, F_SETFL, O_NONBLOCK);
	// Allocate the buffer for a single packet.
	unsigned char packet[32767];
	unsigned char packetOut[32767];

	int timer = 0;

/*	fd_set set_read;
	FD_ZERO(&set_read);
	FD_SET(inter, &set_read);
	FD_SET(sock, &set_read);
*/
	pollfd pollFD[2];
	memset(pollFD,0,sizeof(pollfd)*2);
	pollFD[0].fd=sock;
	pollFD[1].fd=inter;
	pollFD[0].events=POLLIN | POLLHUP;
	pollFD[1].events=POLLIN ;


    printf("Socket: %i\n",sock);
    printf("Interface: %i\n",inter);
    int max=inter+1;
    if(sock>inter)
	max=sock+1;
    printf("Max: %i\n",max);
int rI=0;
int rL=4;
    while (POLLHUP) {

			int ret=::poll(pollFD,2,-1);

			if (pollFD[1].revents != 0)
			{
				printf("Read from interface\n");
				printf("poll 1 =%i\n",pollFD[1].revents);

				// Read the outgoing packet from the input stream.

				int length = read(inter, packetOut, sizeof(packetOut));
				if (length > 0) {
					// Write the outgoing packet to the tunnel.
					send(sock, packetOut, length, MSG_NOSIGNAL);
				}
				printf("Interface length = %i\n",length);
			}
			if(pollFD[0].revents != 0 )
				{
				printf("Read from tunnel\n");
				printf("poll 0 =%i\n",pollFD[0].revents);
					// Read the incoming packet from the tunnel.
					ret=read(sock,packet+rI,rL);
					printf("tuneel length = %i\n",ret);
					if (ret==0){
						break;
					}

					for (int i=0;i<ret;i++){
						printf("%u", (unsigned char) packet[i]);
						//printf("%s", packet[i]);
					}
					printf("\n");
					if(ret==rL)
					{
					rI=0;
					rL=4;
						int length=packet[2];
						length&=0x00FF;
						length<<=8;
						length+=packet[3];
						length-=4;
						printf("%i, Length \n", length);
						int k=length;
						int l=0;
						while(k>0)
						{
							int ret=  read(sock, packet+4+l,k);
							if(ret<0)
        					continue;
							if (ret == k) {
								break;

							}
							k-=ret;
							l+=ret;
						}
						if (length > 0) {
//									printf("write to interface (length=%i)\n",length);

							// Ignore control messages, which start with zero.
							if (write(inter, packet, length + 4) != length + 4)
							{
								perror("Error writing to TUN interface - 1!");

							}

						}
					}
					else{
					rL-=ret;
					rI+=ret;
					}
				}

			 }

    printf("%s: The tunnel is broken\n");
    close(sock);
}



int main(int argc, char **argv)
{
    if (argc < 5) {
        printf("Usage: %s <tunN> <port> <secret> options...\n"
               "\n"
               "Options:\n"
               "  -m <MTU> for the maximum transmission unit\n"
               "  -a <address> <prefix-length> for the private address\n"
               "  -r <address> <prefix-length> for the forwarding route\n"
               "  -d <address> for the domain name server\n"
               "  -s <domain> for the search domain\n"
               "\n"
               "Note that TUN interface needs to be configured properly\n"
               "BEFORE running this program. For more information, please\n"
               "read the comments in the source code.\n\n", argv[0]);
        exit(1);
    }

    // Get TUN interface.
    int interface = get_interface(argv[1]);

    // We use an IPv6 socket to cover both IPv4 and IPv6.
    int tunnel = socket(AF_INET6, SOCK_STREAM, 0);

    int flag = 1;
    setsockopt(tunnel, SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
    flag = 0;
    setsockopt(tunnel, IPPROTO_IPV6, IPV6_V6ONLY, &flag, sizeof(flag));

    // Accept packets received on any local address.
    sockaddr_in6 addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin6_family = AF_INET6;
    addr.sin6_port = htons(atoi(argv[2]));


    // Call bind(2) in a loop since Linux does not have SO_REUSEPORT.
    while (bind(tunnel, (sockaddr *)&addr, sizeof(addr))) {
    	if (errno != EADDRINUSE) {
    		return -1;
    	}
    	usleep(100000);
    }

    listen(tunnel,5);

    // Wait for a tunnel.
    while (1) {

    	   socklen_t addrlen;
    	   addrlen = sizeof(addr);
    	   int pid;

    	   int    newsockfd = accept(tunnel,
    			  (sockaddr *)&addr, &addrlen);

    	   printf("%s: Here comes a new tunnel\n", argv[1]);

    	   if (newsockfd < 0)
    		   printf("%s: ERROR on accept\n");

    	   pid = fork();
    	   if (pid < 0)
    		   printf("%s: ERROR on fork\n");
    	   if (pid == 0)  {
    	           close(tunnel);
    	           vpnprotocol(newsockfd,interface);
    	           exit(0);
    	    }
    	    else close(newsockfd);

    }
    close(tunnel);
    perror("Cannot create tunnels");
    exit(1);
}

