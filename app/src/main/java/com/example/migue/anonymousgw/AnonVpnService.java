package com.example.migue.anonymousgw;

/**
 * Created by migue on 16.06.15.
 * handles a vpn connection
 */
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.VpnService;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import anon.AnonChannel;
import anon.client.AnonClient;
import anon.crypto.SignatureVerifier;
import anon.infoservice.MixCascade;
import anon.infoservice.SimpleMixCascadeContainer;
import anon.transport.connection.IStreamConnection;
import anon.transport.connection.SocketConnection;


public class AnonVpnService extends VpnService implements Handler.Callback {
    private Thread mThread;
    private ParcelFileDescriptor mInterface;
    Builder builder = new Builder();
    private String mParameters;
    private static final String TAG = "ANONVPN";
    private String mServerAddress;
    private String mServerPort;
    private PendingIntent mConfigureIntent;
    private Handler mHandler;
    private static Anondata mAnondata= new Anondata();
    private boolean mStarted = false;
    private boolean mDisableAnon = false;
    private boolean mUseMixes = false;
    private static AnonIpHeaderWrapper ipheaderInbound = new AnonIpHeaderWrapper(null);
    private static AnonIpHeaderWrapper ipheaderOutbound = new AnonIpHeaderWrapper(null);
    private final ConnectivityBroadcastStatus mbroadcastReceiver = new ConnectivityBroadcastStatus();

    // Services interface
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Start a new session by creating a new thread.
        String prefix = getPackageName();
        mServerAddress = intent.getStringExtra(prefix + ".ADDRESS");
        mServerPort = intent.getStringExtra(prefix + ".PORT");
        if (mHandler == null) {
            mHandler = new Handler(this);
        }
        if (mAnondata == null) {
            mAnondata = new Anondata();
        }
        startAnonVPN(mUseMixes);
        mbroadcastReceiver.register();
        // service running until it is explicitly stopped,return sticky.
        return START_STICKY;
    }

    public void startAnonVPN(final boolean bUseMixes){
        if (! mStarted)
        {
            mThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    InetSocketAddress isa = new InetSocketAddress(mServerAddress,Integer.valueOf(mServerPort));
                    try {

                        Socket socketToProtect=null;
                        final OutputStream tunnelOut;
                        final InputStream tunnelIn;
                        final int iOutputBlockSize;
                        final int mtu;

                        if(bUseMixes)
                        {
                            AnonClient anon=new AnonClient(null); //create AN.ON client

                            //--> set to your cascde here!!
                            MixCascade mixCascade=new MixCascade(null, null, "10.0.0.1", 6544); //manually select mix cascade

                            SignatureVerifier.getInstance().setCheckSignatures(false); //insecure, of course - but necessary for this simple manually added mix-cascade

                            //initialize the client --> establish the connection
                            anon.initialize(mixCascade, new SimpleMixCascadeContainer(mixCascade), null, true);

                            //create a communication channel (like a connected socket)
                            AnonChannel c=anon.createChannel(AnonChannel.HTTP);
                            iOutputBlockSize=c.getOutputBlockSize();
                            mtu=iOutputBlockSize*50;
                            tunnelIn=c.getInputStream();
                            tunnelOut=c.getOutputStream();
                            IStreamConnection con=anon.getConnectionToMixCascade().getUnderlyingIStreamConnection();
                            SocketConnection scon=(SocketConnection)con;
                            socketToProtect=scon.getUnderlyingSocket();
                        }
                        else //direct connection to VPN Server
                        {
                            SocketChannel sc = SocketChannel.open();
                            sc.connect(isa);
                            sc.configureBlocking(true);
                            tunnelOut=sc.socket().getOutputStream();
                            tunnelIn=sc.socket().getInputStream();
                            socketToProtect=sc.socket();
                            iOutputBlockSize=0x00FFFF;
                            mtu=1400;
                        }

                        if (!protect(socketToProtect)) {
                            throw new IllegalStateException("Cannot protect the tunnel");
                        }
                        //set a random IP address and start off
                        String response =   handshake(mtu);
                        mStarted=true;
                        Message msg = Message.obtain();
                        //Transparency, telling the user which IP is broadcasting when browsing
                        msg.obj = "VPN Connected, your new IP: "+response;
                        msg.setTarget(mHandler);
                        msg.sendToTarget();

                        final Runnable initRunnable = new Runnable() {
                            public void run() {
                                FileInputStream in = new FileInputStream(mInterface.getFileDescriptor());
                                ByteBuffer packet=ByteBuffer.allocate(32767);
                                byte[] arPacket=packet.array();
                                int length=0,tcpudpdataHeaderlength=0;
                                byte[] checksums = new byte[8];
                                while (true) {
                                    try {
                                        if((length=in.read(packet.array())) > 0 && ipheaderInbound.setIPheader(packet.array()) && (ipheaderInbound.protocol() == 17 || ipheaderInbound.protocol() == 6 ) && (ipheaderInbound.HdrLenghtIp()==5)  ) {
//                                        if((length=in.read(packet.array())) > 0 && ipheaderInbound.setIPheader(packet.array())) {
                                            // Write the outgoing packet to the tunnel.
                                            //check if some packets are leaking out the real IP because the VPN was broken or not operating
                                            if(!mDisableAnon) {

                                                tcpudpdataHeaderlength = (ipheaderInbound.HdrLenghtIp() * 4);

                                                //Clean checksums both  layers..
                                                if (ipheaderInbound.protocol() == 17) {
                                                    arPacket[tcpudpdataHeaderlength + 6] = (byte) (0x0000);
                                                    arPacket[tcpudpdataHeaderlength + 7] = (byte) (0x0000);

                                                }
                                                if (ipheaderInbound.protocol() == 6) {
                                                    arPacket[tcpudpdataHeaderlength + 16] = (byte) (0x0000);
                                                    arPacket[tcpudpdataHeaderlength + 17] = (byte) (0x0000);
                                                }
                                                arPacket[10] = (byte) (0x0000);
                                                arPacket[11] = (byte) (0x0000);
                                                //End checksums in zeros

                                                //Masquering out source ports...
                                                if (!mAnondata.isMapped(ipheaderInbound.sourcePort())) {
                                                    //map the source port
                                                    short portmapped = mAnondata.mapSourceport(ipheaderInbound.sourcePort());
                                                    Log.i(TAG, "Mapping port " + ipheaderInbound.sourcePort() + " port mapped "+portmapped);
                                                }
                                                //sould be an else sentence but not bcause its not thread safe
                                                if (mAnondata.isMapped(ipheaderInbound.sourcePort())) {
                                                    //Mask the original port by a random port
                                                    short portmapped=ipheaderInbound.sourcePort();
                                                    short value=mAnondata.getSourceportmapped(ipheaderInbound.sourcePort());

                                                    ByteBuffer buffer = ByteBuffer.allocate(2);
                                                    buffer.putShort(value);

                                                    arPacket[tcpudpdataHeaderlength+1] = buffer.get(1);
                                                    arPacket[tcpudpdataHeaderlength] = buffer.get(0);
                                                    Log.i(TAG, "testing port " + ipheaderInbound.sourcePort() + " port mapped "+portmapped);

                                                }

                                                //change IDs
                                                arPacket[4]=(byte) (0x0000);
                                                arPacket[5]=(byte) (0x0000);


                                                //restore checksum Layers
                                                short valuec = ipheaderInbound.getChecksumcalculateTwo();
                                                packet.put(11, (byte) (valuec & 0xff));
                                                valuec = (short) (valuec >> 8);
                                                packet.put(10, (byte) (valuec));

                                                if (ipheaderInbound.protocol() == 17) {
                                                    short value = ipheaderInbound.getChecksumcalculate();
                                                    packet.put(tcpudpdataHeaderlength + 7, (byte) (value & 0xff));
                                                    value = (short) (value >> 8);
                                                    packet.put(tcpudpdataHeaderlength + 6, (byte) (value));

                                                }
                                                if (ipheaderInbound.protocol() == 6) {
                                                    short value = ipheaderInbound.getChecksumcalculate();
                                                    packet.put(tcpudpdataHeaderlength + 17, (byte) (value & 0xff));
                                                    value = (short) (value >> 8);
                                                    packet.put(tcpudpdataHeaderlength + 16, (byte) (value));

                                                }
                                                if (ipheaderInbound.protocol()==17 || ipheaderInbound.protocol()==6){
                                                    Log.i("ANONSTATS","protocol "+ipheaderInbound.protocol()+" checksumIP: [ "+ipheaderInbound.getChecksumIP()+ " / " + ipheaderInbound.getChecksumcalculateTwo()+" ]"+
                                                            "checsumsTCPUDP: ["+ipheaderInbound.getChecksum(ipheaderInbound.protocol())+ " / " + ipheaderInbound.getChecksumcalculate() +" ]");
                                                }
                                            }

                                            if(length<= iOutputBlockSize) {
                                                tunnelOut.write(arPacket, 0, length);
                                            }
                                            else
                                            {
                                                int l=length;
                                                int p=0;
                                                while (l>0)
                                                {
                                                    int k=Math.min(l,iOutputBlockSize);
                                                    tunnelOut.write(arPacket,p,k);
                                                    l-=k;
                                                    p+=k;
                                                }
                                            }

                                           /* packet.limit(length);
                                            packet.position(0);
                                            while (packet.hasRemaining()) sc.write(packet);*/
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        };
                        Thread initThread = new Thread(initRunnable);
                        initThread.setDaemon(true);
                        initThread.setName("ANONdroid service - Sending");
                        initThread.start();
/*
                        FileOutputStream out = new FileOutputStream(mInterface.getFileDescriptor());
                        ByteBuffer packetIn=ByteBuffer.allocate(32767);
                        byte[] arPacketIn=packetIn.array();

                        packetIn.position(0);
                        packetIn.limit(4);
                        int lengthIn=0,tcpudpdataHeaderlength=0;*/

                        FileOutputStream out = new FileOutputStream(mInterface.getFileDescriptor());
                        ByteBuffer packetIn=ByteBuffer.allocate(0x00FFFF);
                        byte[] arPacketIn=packetIn.array();


                        //packetIn.position(0);
                        //packetIn.limit(4);
                        int lengthIn=0,tcpudpdataHeaderlength=0;
                        int posIn=0;
                        int remaining=4;


                        while (true) {
                            // Read the incoming packet from the tunnel.
                     /*       if(packetIn.hasRemaining())
                                sc.read(packetIn);
                            if(packetIn.position()==4&&lengthIn==0) {
                                lengthIn = arPacketIn[2];
                                lengthIn <<= 8;
                                lengthIn &= 0x00FF00;
                                lengthIn += (arPacketIn[3] & 0x00FF);
                                packetIn.limit(lengthIn);
                                sc.read(packetIn);
                            }
                            if(lengthIn>0) {
                                if (packetIn.hasRemaining())
                                    sc.read(packetIn);
                                else {*/


                            // Read the incoming packet from the tunnel.
                            if(remaining>0) {
                                int l=tunnelIn.read(arPacketIn,posIn,remaining);
                                remaining-=l;
                                posIn+=l;
                            }
                            if(posIn==4&&lengthIn==0) {
                                //AnonIpHeaderWrapper ipheader = new AnonIpHeaderWrapper(arPacketIn);
                                lengthIn = ((arPacketIn[2]&0xFF)<<8) | (arPacketIn[3]&0xFF);
                                remaining=lengthIn-4;
                                int l=tunnelIn.read(arPacketIn,posIn,remaining);
                                remaining-=l;
                                posIn+=l;
                            }
                            if(lengthIn>0) {
                                if(remaining>0) {
                                    int l=tunnelIn.read(arPacketIn,posIn,remaining);
                                    remaining-=l;
                                    posIn+=l;
                                }
                                else {
                                    if ((!mDisableAnon) && ipheaderOutbound.setIPheader(packetIn.array()) && mAnondata.isMappedDest(ipheaderOutbound.destPort())) {

                                        tcpudpdataHeaderlength=(ipheaderOutbound.HdrLenghtIp()*4);

                                        Log.i(TAG, "Receiving mapped " + ipheaderOutbound.destPort() + " port mapped ");


//                                        Log.i(TAG, " Rev port mapped: "+packetIn.get(tcpudpdataHeaderlength+2)+"--"+packetIn.get(tcpudpdataHeaderlength+3)+"::" +(byte)((short)(mAnondata.getDestportmapped(ipheaderOutbound.destPort()) & 0x00FF))+"-"+(byte)((short)((mAnondata.getDestportmapped(ipheaderOutbound.destPort()) & 0xFF00)>>8)) + " Dest Port: " + ipheaderOutbound.destPort() + " Dest port map: " + mAnondata.getDestportmapped(ipheaderOutbound.destPort()) + " Source Port " + ipheaderOutbound.sourcePort() + " Destination " + ipheaderOutbound.getDestination() + " Source " + ipheaderOutbound.getSource() + " Protocol " + ipheaderOutbound.protocol() +" FIN ARRAY " + lengthIn);

                                        //Clean checksums both  layers..
                                        if (ipheaderOutbound.protocol() == 17) {
                                            arPacketIn[tcpudpdataHeaderlength + 6] = (byte) (0x0000);
                                            arPacketIn[tcpudpdataHeaderlength + 7] = (byte) (0x0000);

                                        }
                                        if (ipheaderOutbound.protocol() == 6) {
                                            arPacketIn[tcpudpdataHeaderlength + 16] = (byte) (0x0000);
                                            arPacketIn[tcpudpdataHeaderlength + 17] = (byte) (0x0000);
                                        }
                                        arPacketIn[10] = (byte) (0x0000);
                                        arPacketIn[11] = (byte) (0x0000);
                                        //End checksums in zeros

                                        //re-set the Original port

                                        ByteBuffer buffer = ByteBuffer.allocate(2);
                                        buffer.putShort(mAnondata.getDestportmapped(ipheaderOutbound.destPort()));

                                        arPacketIn[tcpudpdataHeaderlength + 3]= buffer.get(1);
                                        arPacketIn[tcpudpdataHeaderlength + 2]= buffer.get(0);

                                        //restore checksum Layers
                                        short valuec = ipheaderOutbound.getChecksumcalculateTwo();
                                        packetIn.put(11, (byte) (valuec & 0xff));
                                        valuec = (short) (valuec >> 8);
                                        packetIn.put(10, (byte) (valuec));

                                        if (ipheaderOutbound.protocol() == 17) {
                                            short value = ipheaderOutbound.getChecksumcalculate();
                                            packetIn.put(tcpudpdataHeaderlength + 7, (byte) (value & 0xff));
                                            value = (short) (value >> 8);
                                            packetIn.put(tcpudpdataHeaderlength + 6, (byte) (value));

                                        }
                                        if (ipheaderOutbound.protocol() == 6) {
                                            short value = ipheaderOutbound.getChecksumcalculate();
                                            packetIn.put(tcpudpdataHeaderlength + 17, (byte) (value & 0xff));
                                            value = (short) (value >> 8);
                                            packetIn.put(tcpudpdataHeaderlength + 16, (byte) (value));

                                        }
                                    } else
                                        Log.i(TAG, "Receiving mapped Invalid " + ipheaderOutbound.destPort() + " port mapped ");
                                    /*out.write(arPacketIn, 0, lengthIn);
                                    packetIn.position(0);
                                    packetIn.limit(4);
                                    lengthIn=0;*/

                                    out.write(arPacketIn, 0, lengthIn);
                                    posIn=0;
                                    remaining=4;
                                    lengthIn=0;
                                }
                            }

                        }

                    } catch (Exception e) {
                        // Catch any exception
                        e.printStackTrace();
                    } finally {
                        try {
                            if (mInterface != null) {
                                mInterface.close();
                                mInterface = null;
                                mbroadcastReceiver.unregister();
                            }
                            //sc.close();
                            mStarted=false;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

            }, "ANONdroid service - Receiving");
            //start the service
            Log.i("CHECK", "VPN started");
            mThread.start();

        }
    }



    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        Log.i("CHECK", "VPN closed");
        if (mThread != null) {
            mThread.interrupt();
        }
        super.onDestroy();
    }
    private String handshake(int mtu) throws Exception {

        //generating a random number between 2 and 254 on each octect
        int min=2;
        int max=254;
        String ip="10.";
        ip = ip+Integer.toString(min + (int)(Math.random() * ((max - min) + 1)))+".";
        ip = ip+Integer.toString(min + (int)(Math.random() * ((max - min) + 1)))+".";
        ip = ip+Integer.toString(min + (int)(Math.random() * ((max - min) + 1)));
        // not route configured and to avoid DNS leaking a dns 8.8.8.8 is setup
        configure(mtu, new String("a,"+ip+",32 d,8.8.8.8 r,0.0.0.0,0"));
        mAnondata.setIPvpn(ip);
        Log.i(TAG, "IPs: " + mAnondata.getlocalips());
        return ip;


    }



    private void configure(int mtu,String parameters) throws Exception {
        // Configure a builder while parsing the parameters.
        Builder builder = new Builder();
        builder.setMtu(mtu);
        for (String parameter : parameters.split(" ")) {
            String[] fields = parameter.split(",");
            try {
                switch (fields[0].charAt(0)) {
                    case 'a':
                        builder.addAddress(fields[1], Integer.parseInt(fields[2]));
                        break;
                    case 'r':
                        builder.addRoute(fields[1], Integer.parseInt(fields[2]));
                        break;
                    case 'd':
                        builder.addDnsServer(fields[1]);
                        break;
                    case 's':
                        builder.addSearchDomain(fields[1]);
                        break;
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("Bad parameter: " + parameter);
            }
        }
        // Close the old interface since the parameters have been changed.
        try {
            mInterface.close();
        } catch (Exception e) {
            // ignore
        }
        // Create a new interface using the builder and save the parameters.
        mInterface = builder.setSession(mServerAddress)
                .setConfigureIntent(mConfigureIntent)
                .establish();
        mParameters = parameters;
        Log.i(TAG, "New interface: " + parameters);
    }


    @Override
    public boolean handleMessage(Message message) {
        if (message != null) {
            Toast.makeText(this,(String) message.obj, Toast.LENGTH_LONG).show();
        }
        return true;
    }
    public class ConnectivityBroadcastStatus extends BroadcastReceiver
    {
        private boolean _receiverRegistered = false;

        /**
         * Listen for network change events, and force reconnection as soon as connectivity is back.
         */
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String aAction = intent.getAction();
            if(aAction.equals(ConnectivityManager.CONNECTIVITY_ACTION))
            {
                // Check if we have connectivity
                boolean aConnectivity = ! intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
                Log.d(TAG, "Network state changed - network available? " + aConnectivity);
                if (aConnectivity)
                {
                    Log.i(TAG, "Network state changed - forcing reconnection");
                    startAnonVPN(mUseMixes);

                }
            }
        }

        /**
         * Register a broadcast receiver to get notified on network state change.
         */
        public void register()
        {
            // Only register if anonVPN is started
            if ( mStarted)
            {
                IntentFilter aFilter = new IntentFilter();
                aFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
                registerReceiver(mbroadcastReceiver, aFilter);
                _receiverRegistered = true;
            }
        }

        /**
         * Unregister broadcast receiver.
         */
        public void unregister()
        {
            if (_receiverRegistered)
            {
                unregisterReceiver(mbroadcastReceiver);
                _receiverRegistered = false;
            }
        }
    };



}