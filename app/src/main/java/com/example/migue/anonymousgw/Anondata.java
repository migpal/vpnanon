package com.example.migue.anonymousgw;

import android.util.Log;

import org.apache.http.conn.util.InetAddressUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by migue on 28.09.15.
 * data for anonymity
 */
public class Anondata {
    private HashMap<String,String> localips = new  HashMap<String,String>();
    private HashMap<Short,Short> sourceports = new HashMap<Short,Short>();
    private HashMap<Short,Short> destports = new HashMap<Short,Short>();
    private  HashMap<Short,Short> numberFiltered = new HashMap<Short,Short>();
    // a dummy seed
    private short mapport=15;
    // a dummy seed for identification, shoould be random
    private int idmapped=1;

    byte[] ipvpn=new byte[4];

    public short getSourceportmapped(short port){
        return sourceports.get(port);
    }
    public short getidmapped(int ID){
        return 0;
    }
    public int getidoriginal(int ID){
        //return identificationin.get(ID);
        return 0;
    }



    public short getDestportmapped(short port){
        return destports.get(port);
    }

    public String getIPvpn() {
        return localips.get("ipVpn");
    }
    public short mapSourceport(short port){


        int min=2000;
        int max=15000;
        mapport= (short) (min + (int)(Math.random() * ((max - min) + 1)));

        sourceports.put(port,mapport);
        destports.put(mapport,port);

        mapport++;
        return mapport;
    }



    public Boolean isMapped(short port){
        if (sourceports.containsKey(port)){
            return true;
        }
        return false;
    }


    public Boolean isMappedDest(short port){
        if (destports.containsKey(port)){
            return true;
        }
        return false;
    }




    public byte[] getIPvpnbytes() {
        return ipvpn;
    }

    public void setIPvpn(String IPvpn) {
        localips.put("ipVpn",IPvpn);
        int i=0;
        for (String octet : IPvpn.split(Pattern.quote("."))){
            ipvpn[i]=Integer.valueOf(octet).byteValue();
            i++;
        }
    }

    public String getlocalips(){
        return localips.toString();
    }

    public Anondata(){
        ipInterfaces();
    }
    public void ipInterfaces(){
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && InetAddressUtils.isIPv4Address(inetAddress.getHostAddress())){
                        localips.put(Integer.toString(inetAddress.hashCode()),inetAddress.getHostAddress());
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }

    }

}
