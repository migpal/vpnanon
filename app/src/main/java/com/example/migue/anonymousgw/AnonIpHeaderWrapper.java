package com.example.migue.anonymousgw;

import android.text.format.Formatter;
import android.util.Log;

import org.apache.http.conn.util.InetAddressUtils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

/**
 * Created by migue on 08.09.15.
 * handles all IP headers
 */
public class AnonIpHeaderWrapper {
    ByteBuffer buf;

    public AnonIpHeaderWrapper(byte[] data)
    {
        setIPheader(data);
    }

    public boolean setIPheader(byte[] data){
        try {
            this.buf = ByteBuffer.wrap(data);
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public short getVersionIp()
    {
        return (byte) ((buf.get(0) & 0xF0) >> 4);
    }
    public short HdrLenghtIp()
    {
        return (byte) (buf.get(0) & 0x0F);
    }
    public short typeOfService(){
        return (byte) (buf.get(1));
    }
    public short getIdentification(){
        return (short) (((buf.get(4)&0xFF)<<8) | (buf.get(5)&0xFF));
    }

    public int lengthIp(){

        return (int)( ((buf.get(2)&0xFF)<<8) | (buf.get(3)&0xFF) );

    }
    public short fragmentSeqIp(){

        return (short)( ((buf.get(6)&0xFF)<<8) | (buf.get(7)&0xFF) );

    }
    public short flagfragmentIp(){

        return (byte)  (buf.get(6) & 0x0F);

    }
    public short TTL(){
        return (byte)  (buf.get(8));
    }
    public short protocol(){
        return (byte)  (buf.get(9));
    }

    public String getSource()
    {
        return (buf.get(12) & 0xFF)+ "."+(buf.get(13) & 0xFF)+"."+(buf.get(14) & 0xFF)+"."+(buf.get(15) & 0xFF);

    }

    public String getDestination()
    {
        return  (buf.get(16) & 0xFF) + "."+(buf.get(17) & 0xFF)+"."+(buf.get(18) & 0xFF)+"."+(buf.get(19) & 0xFF);
    }

    public short getChecksum(int proto)
    {
        if (proto==17)
            return (short)( ((buf.get(6)&0xFF)<<8) | (buf.get(7)&0xFF) );
        if (proto==6)
            return (short)( ((buf.get(16)&0xFF)<<8) | (buf.get(17)&0xFF) );
        return 3;
    }

    public short getChecksumIP()
    {

        return (short)( ((buf.get(10)&0xFF)<<8) | (buf.get(11)&0xFF) );
    }

    public short getChecksumcalculate()
    {
        short length=0;
        if (protocol()==17 || protocol()==6){
            length =  (short)(lengthIp()-(HdrLenghtIp()*4));
            return checksum(length,(HdrLenghtIp()*4));
        }
        return length;

        //return tcp.checksum(proto);
    }
    public short getChecksumcalculateTwo()
    {
        short length= (short) (HdrLenghtIp()*4);
        return checksumtwo(length, buf.array(), 0);
        //return tcp.checksum(proto);
    }


    public short getUrgentPtr()
    {
        return buf.getShort(18);
    }

    public void setUrgentPtr(short value)
    {
        buf.putShort(18, value);
    }

    public short sourcePort(){
        int IPheader=HdrLenghtIp()*4;
        return (short)(((buf.get(IPheader)&0xFF)<<8) | (buf.get( IPheader+1)&0xFF));
    }
    public short destPort(){
        int IPheader=HdrLenghtIp()*4;
        return (short)( ((buf.get(IPheader+2)&0xFF)<<8) | (buf.get(IPheader+3)&0xFF) );
    }
   /* public int seqNumber(){
        if (protocol()==6){
            return tcp.seqNumber();
        }else
            return 0;

    }
    public int lengthPacket(){
        return tcp.lenghtpacket();
    }

    public String pseudohdr(){
        String s="";
        if (protocol()==17){
            byte[] pseudo=tcp.pseudoHdr();
            for (int i=0;i<pseudo.length;i++){
                s+="-"+pseudo[i];
            }
        }
        return s;

    }*/

    long integralFromBytes( byte[] buffer , int offset , int length ){
        long answer = 0 ;
        while( -- length >= 0 ) {
            answer = answer << 8 ;
            answer |= buffer[offset] >= 0 ? buffer[offset] : 0xffffff00 ^ buffer[offset] ;
            ++ offset ;
        }
        return answer ;
    }

    public short calculateChecksum(byte[] buf) {
        int length = 20;
        int i = 0;

        long summ = 0;
        long data;

        while (length > 1) {

            data = (((buf[i] << 8) & 0xFF00) | ((buf[i + 1]) & 0xFF));
            summ += data;
            // 1's complement carry bit correction in 16-bits (detecting sign extension)
            if ((summ & 0xFFFF0000) > 0) {
                summ = summ & 0xFFFF;
                summ += 1;
            }

            i += 2;
            length -= 2;
        }

        summ = ~summ;
        summ = summ & 0xFFFF;
        return (short) summ;
    }



    public void shortToBytes( short value , byte[] buffer , int offset ){
        buffer[ offset + 1 ] = (byte)( value & 0xff );
        value = (short)( value >> 8 );
        buffer[ offset ] = (byte)( value );
    }

    public short checksum( short  length , int    offset ) {
        int bufferlength = length + 12 ;
        boolean odd = length % 2 == 1 ;
        if( odd ){
            ++ bufferlength ;
        }
        byte[] buffer = new byte[ bufferlength ];

        buffer[0] = buf.get(12);
        buffer[1] = buf.get(13);
        buffer[2] = buf.get(14);
        buffer[3] = buf.get(15);
        buffer[4] = buf.get(16);
        buffer[5] = buf.get(17);
        buffer[6] = buf.get(18);
        buffer[7] = buf.get(19);
        buffer[8] = (byte) 0 ;
        buffer[9] = buf.get(9) ;
        shortToBytes( length , buffer , 10 );
        int i = 11;
        while( ++ i < length + 12 ){
            // buffer[ i ] = message[ i + offset - 12 ];
            buffer[ i ] = buf.get(i+offset-12);
        }
        if( odd ){
            buffer[ i ] = (byte) 0 ;
        }
        return checksum( buffer , buffer.length , 0 );
    }

    public short checksum( byte[] message , int length , int offset ) {
        // Sum consecutive 16-bit words.
        int sum = 0 ;
        while( offset < length - 1 ){
            sum += (int) integralFromBytes( message , offset , 2 );
            offset += 2 ;
        }
        if( offset == length - 1 ){
            sum += ( message[offset] >= 0 ? message[offset] : message[offset] ^ 0xffffff00 ) << 8 ;
        }
        // Add upper 16 bits to lower 16 bits.
        sum = ( sum >>> 16 ) + ( sum & 0xffff );
        // Add carry
        sum += sum >>> 16 ;
        // Ones complement and truncate
        return (short) ~sum ;
    }


    public short checksumtwo( short  length ,
                              byte[] message ,
                              int    offset ) {

        return calculateChecksum(message );
    }



}