package com.example.migue.anonymousgw;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.net.VpnService;
import android.content.Intent;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import static java.util.concurrent.TimeUnit.NANOSECONDS;


/**
 * Created by migue on 15.06.15.
 */
public class AnonVpnClient extends Activity implements View.OnClickListener  {
    private TextView mServerAddress;
    private TextView mServerPort;
    private Thread mThread;
    private long startTime=0;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form);
        mServerAddress = (TextView) findViewById(R.id.address);
        mServerPort = (TextView) findViewById(R.id.port);
        findViewById(R.id.connect).setOnClickListener(this);
        findViewById(R.id.buttonTr).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.connect:
                int i=0;
                Intent intent = VpnService.prepare(getApplicationContext());
                if (intent != null) {
                    startActivityForResult(intent, 0);
                } else {
                    onActivityResult(0, RESULT_OK, null);
                }
                break;
            case R.id.buttonTr:
                runUdpEchoClient();
                break;
        }

    }
    @Override
    protected void onActivityResult(int request, int result, Intent data) {
        if (result == RESULT_OK) {
            String prefix = getPackageName();
            Intent intent = new Intent(this, AnonVpnService.class)
                    .putExtra(prefix + ".ADDRESS", mServerAddress.getText().toString())
                    .putExtra(prefix + ".PORT", mServerPort.getText().toString());
            startService(intent);
        }
        Log.i("CHECK", "RequestCode = " + request + "  ResultCode = " + result + "   Intent Data = " + data);

    }

    private void runUdpEchoClient(){
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    InetAddress serverAddr = InetAddress.getByName("141.76.46.165");
                    SenderThread sender = new SenderThread(serverAddr, 8101);
                    sender.start();
                    Thread receiver = new ReceiverThread(sender.getSocket());
                    receiver.start();
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }, "ANONdroid Testing - EchoClient");
        Log.i("CHECK", "transfer started");
        mThread.start();
    }


    class ReceiverThread extends Thread {
        DatagramSocket socket;

        private boolean stopped = false;

        public ReceiverThread(DatagramSocket ds) throws SocketException {
            this.socket = ds;
        }

        public void halt() {
            this.stopped = true;
        }

        public void run() {
            byte[] buffer = new byte[65507];
            while (true) {
                if (stopped)
                    return;
                DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
                try {
                    socket.receive(dp);
                   // String flag=new String(dp.getData(), 0, dp.getLength());
                    //new String(dp.getData(), 0, dp.getLength()) +
                        Log.i("TcpClient", "Receive Chunk number: "+" TIME ELAPSED: " + (System.nanoTime() - startTime) + System.getProperty("line.separator"));
                   // Thread.yield();
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            }
        }
    }


//166547153
//66003230

    class SenderThread extends Thread {

        private InetAddress server;

        private DatagramSocket socket;

        private boolean stopped = false;

        private int port;

        public SenderThread(InetAddress address, int port) throws SocketException {
            this.server = address;
            this.port = port;
            this.socket = new DatagramSocket();
            this.socket.connect(server, port);
        }

        public void halt() {
            this.stopped = true;
        }

        public DatagramSocket getSocket() {
            return this.socket;
        }

        public void run() {

            try {
                BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

                File sdcard = Environment.getExternalStorageDirectory();
                File file = new File(sdcard,"test2.txt");
                FileInputStream fin =  new FileInputStream(file);
                byte fileContent[] = new byte[32767];
                int chunks=3;
                startTime=System.nanoTime();//START
                Log.i("TcpClient", "#####################");
                while (true) {
                    if (stopped)
                        return;

                    fin.read(fileContent);
                    if (chunks==0){
                        halt();
                    }
                    chunks--;
                    DatagramPacket output = new DatagramPacket(fileContent, fileContent.length, server, port);
                    socket.send(output);
                   // Thread.yield();
                }
            }
            catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }








}
